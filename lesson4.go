// Lesson 4
// - Functions
// - Input/Output
// - Function Types

package main

import (
	"fmt"
	"strings"
)

func lesson4() {
	fmt.Printf("== LESSON 4 ==\n")
	// this function doesn't have input or output
	printHello()
	// we can call functions with passed in arguments
	printName("Jacob")
	// we can also get the return values from a function
	myName := getName()
	fmt.Printf("My name is %v\n", myName)
	// we can skip assigning a variable and use the function call directly
	fmt.Printf("Your full name is %v\n", concatNames("Jacob", "Troxel"))
	// make sure to assign ALL return values
	name, isUpper := upperCaseIfLongName("jacobtroxel")
	fmt.Printf("name = %v, isUpper = %v\n", name, isUpper)
	// we can ignore a value by using _
	otherName, _ := upperCaseIfLongName("jacobtroxel")
	fmt.Printf("name = %v, isUpper = ?\n", otherName)
}

// a basic function has no input or output
func printHello() {
	fmt.Printf("Hello!\n")
}

// functions can have input values called parameters
// we must give each parameter a type
func printName(name string) {
	fmt.Printf("Your name is %v\n", name)
}

// functions can also return a value
// we must give the type of the return value
func getName() string {
	return "HAL 9000"
}

// functions that take multiple values of the same type can give their type together
func concatNames(first, last string) string {
	return first + " " + last
}

// functions can also return multiple values
// make sure to give ALL return types (and put them in parens)
func upperCaseIfLongName(name string) (string, bool) {
	if len(name) > 10 {
		name = strings.ToUpper(name)
		return name, true
	}
	return name, false
}
