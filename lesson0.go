// Lesson 0
// - Hello world
// - Intro to GO
// - Intro to Programming Languages

package main

import "fmt"

func lesson0() {
	fmt.Printf("== LESSON 0 ==\n")
	fmt.Printf("Hello world!\n")
}
