// Lesson 3
// - For Loops
// - While Loops
// - If statements

package main

import "fmt"

func lesson3() {
	fmt.Printf("== LESSON 3 ==\n")
	// For loops take an init, end, step values
	// The variable used in init can be used within the for loop
	for i := 0; i < 10; i++ {
		fmt.Printf("i = %v\n", i)
	}
	// If not declared outside the loop, cannot be accessed
	// this causes an error
	//fmt.Printf("i = %v\n", i)

	// We can also leave out the init and step values
	counter := 0
	for counter < 10 {
		fmt.Printf("counter = %v\n", counter)
		counter++
	}

	// There are no WHILE loops in golang, we use a for
	keepGoing := true
	for keepGoing {
		fmt.Printf("keepGoing is true\n")
		keepGoing = false
	}

	// Without a condition, the loop is forever
	for {
		fmt.Printf("forever loop!\n")
		// Let's just stop this shall we ;)
		break
	}

	// We can also loop over a range from an array
	var mySlice []string
	mySlice = append(mySlice, "A")
	mySlice = append(mySlice, "B")
	mySlice = append(mySlice, "C")
	for index, value := range mySlice {
		fmt.Printf("mySlice[%v] = %v\n", index, value)
	}

	// We can leave the value out if we want
	for index := range mySlice {
		fmt.Printf("mySlice[%v] = ?\n", index)
	}

	// If statements check a boolean statement
	doIf := true
	if doIf {
		fmt.Printf("doIf is true\n")
	}

	// We can also add an else block to run otherwise
	if !doIf {
		fmt.Printf("IF executed\n")
	} else {
		fmt.Printf("ELSE executed\n")
	}

	// Else IF's can be used to add more branches -- ONLY one of the block will execute
	myNum := 1
	if myNum == 0 {
		fmt.Printf("IF executed\n")
	} else if myNum == 1 {
		fmt.Printf("ELSE IF executed\n")
	} else {
		fmt.Printf("ELSE executed\n")
	}

	// We can also define a short statement to execute before out if
	if myBool := true; myBool {
		fmt.Printf("myBool is true\n")
	}

	// The values in the short statement can also be used within the else
	if myBool := false; myBool {
		fmt.Printf("myBool in if = %v\n", myBool)
	} else {
		fmt.Printf("myBool in else = %v\n", myBool)
	}
	// Accessing the value outside is not possible unless it was defined outside the IF block
	// this will caused an error
	// fmt.Printf("myBool = %v", myBool)
}
