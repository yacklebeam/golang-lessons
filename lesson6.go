// Lesson 6
// - Switch/Case

package main

import "fmt"

func lesson6() {
	fmt.Printf("== LESSON 6 ==\n")
	fmt.Printf("getDirection(0) => %v\n", getDirection(0))
	fmt.Printf("getWelcomeMessage(5) => %v\n", getWelcomeMessage(5))
	fmt.Printf("getWelcomeMessage(8) => %v\n", getWelcomeMessage(8))
	fmt.Printf("getHeightMessage(67) => %v\n", getHeightMessage(67))
}

// Switch statements match the value given to a case statement
// In go, there is no fallthrough (so no breaks are needed)
func getDirection(direction int) string {
	switch direction {
	case 0:
		return "NORTH"
	case 1:
		return "EAST"
	case 2:
		return "SOUTH"
	case 3:
		return "WEST"
	default:
		return ""
	}
}

// Switch statements can have more than one case PER block (separated by commas)
func getWelcomeMessage(hour int) string {
	switch hour {
	case 0, 1, 2, 3, 4:
		return "It's Early!"
	case 5, 6, 7, 8, 9, 10, 11:
		return "Good Morning!"
	case 12, 13, 14, 15, 16:
		return "Good Afternoon!"
	case 17, 18, 19, 20:
		return "Good Evening!"
	case 21, 22, 23, 24:
		return "It's Bedtime!"
	default:
		return "What time is it???"
	}
}

// Switch statements can be used without a variable, and they become simple boolean checks
func getHeightMessage(height int) string {
	switch {
	case height < 48:
		return "You're short."
	case height >= 48 && height < 72:
		return "You're average."
	default:
		return "You're tall."
	}
}
