// Lesson 2
// - Arrays
// - Slices

package main

import "fmt"

func lesson2() {
	fmt.Printf("== LESSON 2 ==\n")
	// Arrays must be declared with both type and size
	var A [5]int
	fmt.Printf("A = %v\n", A)
	fmt.Printf("A[2] = %v\n", A[2])
	// Array elements can be accessed (read/write) using the index
	A[2] = 17
	fmt.Printf("A = %v\n", A)
	fmt.Printf("A[2] = %v\n", A[2])
	// Slices are arrays without size
	var B []int
	fmt.Printf("B = %v\n", B)
	// This will be an error
	// B[2] = 19
	// Use append to add to the end of a slice
	B = append(B, 1)
	fmt.Printf("B = %v\n", B)
	// Slices can also be allocated with a size
	C := make([]int, 5)
	fmt.Printf("C = %v\n", C)
	// Now we can access by index within size
	C[2] = 3
	fmt.Printf("C = %v\n", C)
	// We can also still append to the end
	C = append(C, 1)
	fmt.Printf("C = %v\n", C)
}
