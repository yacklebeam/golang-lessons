// Lesson 5
// - Maps
// - Structs

package main

import "fmt"

// structs are user defined types, that have their own types
type person struct {
	name string
	age  int
}

func lesson5() {
	fmt.Printf("== LESSON 5 ==\n")
	// maps are special sorts of arrays, that allow us to use something besides an 0-based int index
	// they are declared similar to slices
	myAgeMap := make(map[string]int)
	fmt.Printf("myAgeMap = %v\n", myAgeMap)
	// to add a value to a map, we use the = operator like with an array/slice
	// make sure you use the right types!
	myAgeMap["bob"] = 35
	myAgeMap["sue"] = 28
	fmt.Printf("myAgeMap = %v\n", myAgeMap)
	// access values using the same format
	fmt.Printf("myAgeMap['bob'] = %v\n", myAgeMap["bob"])
	// accessing a key that doesn't exist just gives us the default value for the type
	fmt.Printf("myAgeMap['dave'] = %v\n", myAgeMap["dave"])
	// we can optionally capture two return values to check for existing
	age, exists := myAgeMap["dave"]
	fmt.Printf("myAgeMap['dave'] = %v, exists = %v\n", age, exists)

	// we can declare structs
	var bob person
	bob.age = 37
	bob.name = "Bob"
	// we can use a struct within a map too
	myEmployeeMap := make(map[int]person)
	myEmployeeMap[1109] = bob
	fmt.Printf("myEmployeeMap = %v\n", myEmployeeMap)
	// structs can also be defined in shorthand
	sue := person{name: "sue", age: 28}
	myEmployeeMap[2981] = sue
	fmt.Printf("myEmployeeMap = %v\n", myEmployeeMap)
}
