// Lesson 1
// - Primitive Types
// - Assigning/Creating variables

package main

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"unsafe"
)

func lesson1() {
	fmt.Printf("== LESSON 1 ==\n")
	// Basic primitives are int, string, bool
	// Also see byte (uint8), int32, int64, uint32, uint64
	// float32, float64
	// complex64, complex128
	var myNumber int
	var myString string
	var myBool bool
	// Assigning values using the = operator
	myNumber = 1
	myString = "Jacob"
	myBool = true
	// Print values using fmt.Printf and %v
	fmt.Printf("myNumber = %v\n", myNumber)
	fmt.Printf("myString = %v\n", myString)
	fmt.Printf("myBool = %v\n", myBool)
	fmt.Println()
	// We can also modify values using basic operators
	myNumber += 2
	myString += " Troxel"
	myBool = !myBool
	fmt.Printf("myNumber = %v\n", myNumber)
	fmt.Printf("myString = %v\n", myString)
	fmt.Printf("myBool = %v\n", myBool)
	fmt.Println()
	// Variables can also be declared without type using :=
	myOtherNumber := 2
	myOtherString := "Troxel"
	myOtherBool := false
	fmt.Printf("myOtherNumber = %v\n", myOtherNumber)
	fmt.Printf("myOtherString = %v\n", myOtherString)
	fmt.Printf("myOtherBool = %v\n", myOtherBool)
}

func lesson1b() {
	fmt.Printf("== LESSON 1b ==\n")
	var aInt int
	var aInt8 int8
	var aInt16 int16
	var aInt32 int32
	var aInt64 int64
	var aBool bool

	aInt = 0
	aInt8 = 0
	aInt16 = 0
	aInt32 = 0
	aInt64 = 0
	aBool = false

	fmt.Printf("sizeof(aInt)   = %v byte(s)\n", unsafe.Sizeof(aInt))
	fmt.Printf("sizeof(aInt8)  = %v byte(s)\n", unsafe.Sizeof(aInt8))
	fmt.Printf("sizeof(aInt16) = %v byte(s)\n", unsafe.Sizeof(aInt16))
	fmt.Printf("sizeof(aInt32) = %v byte(s)\n", unsafe.Sizeof(aInt32))
	fmt.Printf("sizeof(aInt64) = %v byte(s)\n", unsafe.Sizeof(aInt64))
	fmt.Printf("sizeof(aBool) = %v byte(s)\n", unsafe.Sizeof(aBool))
}

func lesson1c() {
	fmt.Printf("== LESSON 1c ==\n")
	var int1 int8
	var int2 int8
	var int3 int8
	var int4 int8

	int1 = 1
	int2 = -1
	int3 = 127
	int4 = -128

	printBinary(int1)
	printBinary(int2)
	printBinary(int3)
	printBinary(int4)
}

func printBinary(i int8) {
	buf := new(bytes.Buffer)
	err := binary.Write(buf, binary.LittleEndian, i)
	if err != nil {
		fmt.Println("binary.Write failed:", err)
	}
	fmt.Printf("%4d = %08b\n", i, buf.Bytes())
}
